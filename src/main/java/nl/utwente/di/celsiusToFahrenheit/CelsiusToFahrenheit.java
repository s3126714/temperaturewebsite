package nl.utwente.di.celsiusToFahrenheit;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * Example of a Servlet that gets a Celsius temperature and returns the Fahrenheit equivalent
 */

public class CelsiusToFahrenheit extends HttpServlet {
    public void init() throws ServletException {
    }
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Fahrenheit Temperature";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius Temperature: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit Temperature: " +
                   Converter.celsiusToFahrenheit(Integer.parseInt(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
}
