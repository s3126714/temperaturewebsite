package nl.utwente.di.celsiusToFahrenheit;

public class Converter {
    public static double celsiusToFahrenheit(int celsius) {
        return (double) (celsius * 9) / 5 + 32;
    }
}
